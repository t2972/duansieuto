<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return   [
            'name'=>'required',
            'tuoi'=>'required',
            'quoctich'=>'required',
            'vitri'=>'required',
            'mucluong'=>'required',
            'avt'=>'mimes:jpeg,jpg,png,JPEG,JPG,PNG|required|max:10000',
        ];
    }

    public function messages()
    {
        return   [
            'required'=>':attribute không được để trống',
            'mimes'=>':attribute không phù hợp',
            'max'=>':attribute không lớn hơn :max',
        ];
    }

    public function attributes()
    {
        return   [
            'name'=>"tên cầu thủ",
            'tuoi'=>"tuổi cầu thủ",
            'quoctich'=>"quốc tịch cầu thủ",
            'vitri'=>'vị trí đá ',
            'mucluong'=>'mức lương',
            'avt'=>"avatar cầu thủ",
        ];
    }
}
