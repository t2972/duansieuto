<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\cauthu;
use App\Http\Requests\CheckRequest;
use Mail;

class SinhVienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("success");
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data_cauthu=cauthu::all();
        return view('index1',compact( 'data_cauthu'));
    }
    public function add(){
        return view("add");
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CheckRequest $request)
    {
        $cauthu = new cauthu();
        $cauthu->name=$request->name;
        $cauthu->tuoi=$request->tuoi;
        $cauthu->quoctich=$request->quoctich;
        $cauthu->vitri=$request->vitri;
        $cauthu->luong=$request->mucluong;
        $cauthu->avatar=$request->avt;
        $cauthu->save();
        return view("success");       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $data_cauthu=cauthu::where("id",$id)->get();
        return view("edit",compact("data_cauthu"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CheckRequest $request,$id)
    {
        $cauthu = cauthu::find($id);
                $cauthu->name=$request->name;
                $cauthu->tuoi=$request->tuoi;
                $cauthu->quoctich=$request->quoctich;
                $cauthu->vitri=$request->vitri;
                $cauthu->luong=$request->mucluong;
                $cauthu->avatar=$request->avt;
                $cauthu->save(); 
                return view("success");

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cauthu= cauthu::find($id);
        $cauthu->delete();
        return view("success");

    }
    public function mail(){
        $data=[
            'name'=>'trọng',
            'age'=>25,
        ];
        Mail::send('Testmail',$data,function($message){
            $message->from('nguyennhutrong69@gmail.com','trong');
            $message->to('nguyennhutrong6969@gmail.com','trong');
            $message->subject('thư gửi ông già ngủ quên');
        });
    }
}
