<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>thêm cầu thủ</title>
</head>
<body>
	<form action="{{ url('add') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
		<p>nhập tên cầu thủ</p>
		<input type="text" name="name">
        <p>nhập tuổi cầu thủ</p>
		<input type="text" name="tuoi">
        <p>nhập quốc tịch cầu thủ</p>
		<input type="text" name="quoctich">
        <p>nhập vị trí cầu thủ</p>
		<input type="text" name="vitri">
        <p>nhập mức Lương cầu thủ</p>
		<input type="text" name="mucluong">
        <p></p>
        <input type="file" name="avt"/>
        <input type="submit" name="insert" value="Upload"/></p>
        @if($errors->any())
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        @endif
	</form>
</body>
</html>
