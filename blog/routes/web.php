<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index',"SinhVienController@create");
Route::get('/add',"SinhVienController@add");
Route::post('/add',"SinhVienController@store");

Route::get('/{id}/edit',"SinhVienController@show");
Route::post('/{id}/edit',"SinhVienController@edit");

Route::get('/{id}/delete',"SinhVienController@destroy");

Route::get('/send-mail',"SinhVienController@mail");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
