<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCauthu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cauthu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('tuoi');
            $table->string('quoctich');
            $table->string('vitri');
            $table->string('luong');
            $table->string('avatar');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cauthu');
    }
}
